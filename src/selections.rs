pub use crate::prelude::*;

pub struct Selections {
    pub player_squad: Vec<Id>,
    pub selected_unit: Option<(Entity, Id)>,
    // pub selected_target: Option<Entity>,
}
impl Selections {
    pub fn change_selection(&mut self, entity: &Entity) {
        self.selected_unit = Some((*entity, Id(1)));
    }

    pub fn change_selection_by_id(&mut self, ecs: &mut SubWorld, num: usize) {
        let unit_to_select = <(Entity, &Id)>::query()
            .filter(component::<PlayerControlled>())
            .iter(ecs)
            .find(|(_, id)| id.0 == num);

        let (entity, id) = unit_to_select.unwrap();
        self.selected_unit = Some((*entity, *id));
    }
    pub fn next(&mut self, ecs: &mut SubWorld) {
        if let Some((_, id)) = self.selected_unit {
            let mut to_select = id.0 + 1;

            if id.0 == self.get_squad_len() {
                to_select = 1;
            }

            self.change_selection_by_id(ecs, to_select);
        }
    }

    fn get_squad_len(&self) -> usize {
        self.player_squad.len()
    }
}
