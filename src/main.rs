mod components;
mod input_mode;
mod selections;
mod spawner;
mod systems;

mod prelude {
    pub use crate::components::*;
    pub use crate::input_mode::*;
    pub use crate::selections::*;
    pub use crate::spawner::*;
    pub use crate::systems::*;

    pub use bracket_lib::prelude::*;
    pub use legion::systems::CommandBuffer;
    pub use legion::world::SubWorld;
    pub use legion::*;

    pub const SCREEN_WIDTH: i32 = 80;
    pub const SCREEN_HEIGHT: i32 = 50;
}

use prelude::*;

struct State {
    ecs: World,
    systems: Schedule,
    resources: Resources,
}
impl State {
    fn new() -> Self {
        let systems = build_schedule();

        let mut ecs = World::default();
        let mut resources = Resources::default();

        resources.insert(InputMode {
            mode: InputType::Standard,
        });

        spawn_player_squad(&mut ecs);

        let mut player_squad = Vec::new();

        <&Id>::query()
            .filter(component::<PlayerControlled>())
            .iter(&ecs)
            .for_each(|unit_id| {
                player_squad.push(*unit_id);
            });

        resources.insert(Selections {
            player_squad,
            selected_unit: None,
            // selected_target: None,
        });

        Self {
            ecs,
            systems,
            resources,
        }
    }
}
impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        ctx.set_active_console(0);
        ctx.cls();
        ctx.set_active_console(1);
        ctx.cls();
        ctx.set_active_console(2);
        ctx.cls();

        self.resources.insert(ctx.key);

        self.systems.execute(&mut self.ecs, &mut self.resources);

        render_draw_buffer(ctx).expect("Render Error!");
    }
}

fn main() -> BError {
    let gs = State::new();

    let ctx = BTermBuilder::new()
        .with_title("Tactical Strike")
        .with_font("terminal8x8.png", 8, 8)
        .with_dimensions(SCREEN_WIDTH, SCREEN_HEIGHT)
        .with_tile_dimensions(16, 16)
        .with_fps_cap(30.0)
        .with_simple_console(SCREEN_WIDTH, SCREEN_HEIGHT, "terminal8x8.png")
        .with_simple_console_no_bg(SCREEN_WIDTH, SCREEN_HEIGHT, "terminal8x8.png")
        .with_simple_console_no_bg(SCREEN_WIDTH, SCREEN_HEIGHT, "terminal8x8.png")
        .build()?;

    main_loop(ctx, gs)
}
