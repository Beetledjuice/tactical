use crate::prelude::*;

const SQUAD_MAX_SIZE: i32 = 5;

const NAME_LIST: &[&str] = &[
    "Jonesy", "Adams", "Bubba", "Stephen", "Ollie", "Gomez", "Gurney", "Ilya", "Morgan",
];

pub fn spawn_player_squad(ecs: &mut World) {
    for i in 0..SQUAD_MAX_SIZE {
        let mut random = RandomNumberGenerator::new();

        let mut unit_name = NAME_LIST[random.range(0, NAME_LIST.len())];

        <&Name>::query()
            .filter(component::<PlayerControlled>())
            .iter(ecs)
            .for_each(|name| {
                while unit_name == name.0 {
                    unit_name = NAME_LIST[random.range(0, NAME_LIST.len())];
                }
            });

        let pos = Point::new(
            SCREEN_WIDTH / 2 - SQUAD_MAX_SIZE + (i * 2),
            SCREEN_HEIGHT - 5,
        );
        ecs.push((
            PlayerControlled,
            Name(format!("{}", unit_name)),
            Movable {
                target_pos: pos,
                is_moving: false,
            },
            pos,
            Render {
                color: ColorPair::new(CORNFLOWERBLUE, BLACK),
                glyph: to_cp437('@'),
            },
            Id(i as usize + 1),
        ));
    }
}

pub fn spawn_cursor(commands: &mut CommandBuffer, pos: Point) {
    commands.push((
        Cursor,
        pos,
        Render {
            color: ColorPair::new(YELLOW, BLACK),
            glyph: to_cp437('+'),
        },
    ));
}

pub fn despawn_cursor(ecs: &mut SubWorld, commands: &mut CommandBuffer) {
    let cursor = <Entity>::query()
        .filter(component::<Cursor>())
        .iter(ecs)
        .next()
        .unwrap();

    commands.remove(*cursor);
}
