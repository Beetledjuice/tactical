use crate::prelude::*;

#[system]
pub fn print_commands() {
    let mut db = DrawBatch::new();
    db.target(0);

    draw_box(&mut db);
    // write_commands(&mut db);

    db.submit(0)
        .expect("Batch Error! -- On ui::print_commands()");
}

#[system]
#[read_component(Name)]
#[read_component(Id)]
pub fn print_selected_info(ecs: &mut SubWorld, #[resource] selection: &Selections) {
    let mut db = DrawBatch::new();
    db.target(0);

    print_selected_name(ecs, selection, &mut db);

    db.submit(0).expect("Batch error!");
}

fn print_selected_name(ecs: &mut SubWorld, selection: &Selections, db: &mut DrawBatch) {
    <(&Id, &Name)>::query()
        .filter(component::<PlayerControlled>())
        .iter(ecs)
        .for_each(|(id, name)| {
            if let Some((_, selected_id)) = selection.selected_unit {
                if *id == selected_id {
                    db.print(Point::new(2, SCREEN_HEIGHT - 2), &format!("{}", name.0));
                }
            }
        });
}

fn draw_box(db: &mut DrawBatch) {
    db.draw_box(
        Rect::with_size(0, SCREEN_HEIGHT - 3, SCREEN_WIDTH - 1, 2),
        ColorPair::new(WHITESMOKE, BLACK),
    );
}

fn write_commands(db: &mut DrawBatch) {
    let pos = Point::new(5, SCREEN_HEIGHT - 2);
    db.printer_with_z(
        pos,
        "#[]M#[blue]ove",
        TextAlign::Right,
        Some(RGBA::named(BLACK)),
        10,
    );
}
