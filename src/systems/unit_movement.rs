use crate::prelude::*;

#[system]
#[read_component(Id)]
#[write_component(Movable)]
#[write_component(Point)]
pub fn move_unit(ecs: &mut SubWorld, #[resource] selection: &mut Selections, #[resource] input_mode: &mut InputMode) {
    let mut movables = <(&mut Point, &mut Movable)>::query().filter(component::<PlayerControlled>());
    movables.iter_mut(ecs).for_each(|(pos, mov)|{

        if mov.target_pos != *pos {
            mov.is_moving = true;
            // Calculate distance to target pos
            let mut delta = Point::zero();

            let is_left = mov.target_pos.x < pos.x;
            let is_right = mov.target_pos.x > pos.x;
            let is_above = mov.target_pos.y < pos.y;
            let is_below = mov.target_pos.y > pos.y;

            if is_left {
                delta += Point::new(-1, 0);
            }
            if is_right {
                delta += Point::new(1, 0);
            }
            if is_above {
                delta += Point::new(0, -1);
            }
            if is_below {
                delta += Point::new(0, 1);
            }
            // Take a step in that direction
            if delta.x != 0 || delta.y != 0 {
                *pos += delta;
            }
            // TODO: Wait timer
        } else {
            mov.is_moving = false;
        }

    });

//    if let Some((_, selected_id)) = selection.selected_unit {
//        let mut unit_pos =
//            <(&mut Point, &Id, &mut Movable)>::query().filter(component::<PlayerControlled>());
//
//        unit_pos.iter_mut(ecs).for_each(|(pos, id, mov)| {
//            if id.num == selected_id.num {
//                *pos = mov.target_pos;
//            }
//        });
//    }
}
