use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(Render)]
pub fn entity_render(ecs: &mut SubWorld) {
    let mut draw_batch = DrawBatch::new();
    draw_batch.target(1);

    let mut renderables = <(&Render, &Point)>::query();
    renderables.iter_mut(ecs).for_each(|(render, pos)| {
        draw_batch.set(*pos, render.color, render.glyph);
    });
    draw_batch.submit(10).expect("Batch Error!");
}

#[system]
#[read_component(Point)]
#[read_component(Movable)]
pub fn render_markers(ecs: &mut SubWorld, #[resource] selection: &mut Selections) {
    let left_offset = Point::new(-1, 0);
    let right_offset = Point::new(1, 0);

    let mark_color = ColorPair::new(GREEN, BLACK);

    let mut draw_batch = DrawBatch::new();
    draw_batch.target(2);

    let mut selectables = <(Entity, &Point, &Movable)>::query();
    selectables.iter_mut(ecs).for_each(|(entity, pos, mov)| {
        if let Some((selected_unit, _)) = selection.selected_unit {
            if selected_unit == *entity && !mov.is_moving {
                draw_batch.set(*pos + left_offset, mark_color, to_cp437('<'));
                draw_batch.set(*pos + right_offset, mark_color, to_cp437('>'));
            }
        }
    });
    draw_batch.submit(0).expect("Batch Error!");
}

#[system]
#[read_component(Point)]
#[read_component(Id)]
pub fn render_move_path(
    ecs: &mut SubWorld,
    #[resource] selection: &mut Selections,
    #[resource] input_mode: &InputMode,
) {
    let mut db = DrawBatch::new();

    let mut units = <(&Id, &Point)>::query().filter(component::<PlayerControlled>());
    let mut cursor = <&Point>::query().filter(component::<Cursor>());

    let mut selected_unit_pos = Point::zero();

    if let Some((_, selected_id)) = selection.selected_unit {
        if input_mode.mode == InputType::MoveCursor {
            let cursor_pos = cursor.iter(ecs).next().unwrap();

            units
                .iter(ecs)
                .filter(|(id, _)| **id == selected_id)
                .for_each(|(_, pos)| selected_unit_pos = *pos);

            let path = line2d(LineAlg::Vector, selected_unit_pos, *cursor_pos);

            db.target(0);
            for pos in path {
                if pos != *cursor_pos && pos != selected_unit_pos {
                    db.set(pos, ColorPair::new(GREEN, BLACK), to_cp437('x'));
                }
            }
        }
    }

    db.submit(5).expect("Batch Error");
}
