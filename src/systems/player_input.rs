use crate::prelude::*;

#[system]
#[read_component(Cursor)]
#[write_component(Point)]
#[write_component(Movable)]
#[read_component(Id)]
pub fn selection_input(
    ecs: &mut SubWorld,
    commands: &mut CommandBuffer,
    #[resource] input_mode: &mut InputMode,
    #[resource] selection: &mut Selections,
    #[resource] key: &Option<VirtualKeyCode>,
) {
    match input_mode.mode {
        InputType::Standard => {
            if let Some(key) = key {
                match key {
                    VirtualKeyCode::Tab => {
                        let mut selectables =
                            <Entity>::query().filter(component::<PlayerControlled>());

                        if let Some(unit) = selectables.iter_mut(ecs).next() {
                            if let Some(_) = selection.selected_unit {
                                selection.next(ecs);
                            } else {
                                selection.change_selection(unit);
                            }
                            input_mode.mode = InputType::UnitSelection;
                        }
                    }
                    _ => {}
                }
            }
        }
        InputType::MoveCursor => {
            if let Some(key) = key {
                let mut delta = Point::zero();

                match key {
                    VirtualKeyCode::Numpad8 => {
                        delta = Point::new(0, -1);
                    }
                    VirtualKeyCode::Numpad2 => {
                        delta = Point::new(0, 1);
                    }
                    VirtualKeyCode::Numpad6 => {
                        delta = Point::new(1, 0);
                    }
                    VirtualKeyCode::Numpad4 => {
                        delta = Point::new(-1, 0);
                    }
                    VirtualKeyCode::Return => {
                        if let Some((_, selected_id)) = selection.selected_unit {
                            let cursor_pos = *<&Point>::query()
                                .filter(component::<Cursor>())
                                .iter(ecs)
                                .next()
                                .unwrap();

                            <(&mut Movable, &Id)>::query()
                                .iter_mut(ecs)
                                .for_each(|(mov, id)| {
                                    if *id == selected_id {
                                        mov.target_pos = cursor_pos;
                                    }
                                });
                        }
                        despawn_cursor(ecs, commands);
                        input_mode.mode = InputType::UnitSelection;
                    }
                    VirtualKeyCode::Escape => {
                        if let Some(_) = <&Cursor>::query().iter(ecs).next() {
                            despawn_cursor(ecs, commands);
                            input_mode.mode = InputType::UnitSelection;
                        }
                    }
                    _ => {}
                }

                if delta.x != 0 || delta.y != 0 {
                    let mut cursor = <&mut Point>::query().filter(component::<Cursor>());
                    cursor.iter_mut(ecs).for_each(|pos| {
                        *pos += delta;
                    });
                }
            }
        }
        InputType::UnitSelection => {
            if let Some(key) = key {
                match key {
                    VirtualKeyCode::Tab => {
                        let mut selectables =
                            <Entity>::query().filter(component::<PlayerControlled>());

                        if let Some(unit) = selectables.iter_mut(ecs).next() {
                            if let Some(_) = selection.selected_unit {
                                selection.next(ecs);
                            } else {
                                selection.change_selection(unit);
                            }
                            return;
                        }
                    }
                    VirtualKeyCode::M => {
                        <(&Point, &Id)>::query()
                            .filter(component::<PlayerControlled>())
                            .iter(ecs)
                            .for_each(|(pos, id)| {
                                if let Some((_, selected_id)) = selection.selected_unit {
                                    if *id == selected_id {
                                        spawn_cursor(commands, *pos);
                                        input_mode.mode = InputType::MoveCursor;
                                    }
                                }
                            });
                    }
                    VirtualKeyCode::Key1 => {
                        selection.change_selection_by_id(ecs, 1);
                    }
                    VirtualKeyCode::Key2 => {
                        selection.change_selection_by_id(ecs, 2);
                    }
                    VirtualKeyCode::Key3 => {
                        selection.change_selection_by_id(ecs, 3);
                    }
                    VirtualKeyCode::Key4 => {
                        selection.change_selection_by_id(ecs, 4);
                    }
                    VirtualKeyCode::Key5 => {
                        selection.change_selection_by_id(ecs, 5);
                    }
                    VirtualKeyCode::Escape => {
                        selection.selected_unit = None;
                        input_mode.mode = InputType::Standard;
                    }
                    _ => {}
                };
            }
        }
    }
}
