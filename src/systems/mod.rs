mod player_input;
mod render;
mod ui;
mod unit_movement;

use crate::prelude::*;

pub fn build_schedule() -> Schedule {
    Schedule::builder()
        .add_system(ui::print_commands_system())
        .add_system(ui::print_selected_info_system())
        .flush()
        .add_system(render::entity_render_system())
        .add_system(render::render_markers_system())
        .add_system(render::render_move_path_system())
        .flush()
        .add_system(player_input::selection_input_system())
        .add_system(unit_movement::move_unit_system())
        .build()
}
