use crate::prelude::*;

#[derive(Clone, Copy, PartialEq)]
pub struct PlayerControlled;

#[derive(Clone, PartialEq)]
pub struct Name(pub String);

#[derive(Clone, Copy, PartialEq)]
pub struct Id(pub usize);

#[derive(Clone, Copy, PartialEq)]
pub struct Movable {
    pub target_pos: Point,
    pub is_moving: bool,
}

#[derive(Clone, Copy, PartialEq)]
pub struct Cursor;

#[derive(Clone, Copy, PartialEq)]
pub struct Render {
    pub color: ColorPair,
    pub glyph: FontCharType,
}
