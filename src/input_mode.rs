#[derive(Clone, Copy, PartialEq)]
pub enum InputType {
    Standard,
    UnitSelection,
    MoveCursor,
    // LookCursor,
}

pub struct InputMode {
    pub mode: InputType,
}
